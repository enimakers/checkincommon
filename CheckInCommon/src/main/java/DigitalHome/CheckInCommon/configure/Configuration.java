package DigitalHome.CheckInCommon.configure;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;

public class Configuration {
	protected final static Logger _log = Logger.getLogger(Configuration.class.getName());

	private Properties _properties;
	private String _configFileName;

	public Configuration(String configFileName) throws IOException {

		File file;
		_configFileName = configFileName;

		// Search for configuration file in the same folder as the assembly
		// -----------------------------------------------------------------
		file = new File("./" + configFileName + ".properties");
		if (file.exists()) {
			FileInputStream is = new FileInputStream(file);

			// Create properties obj;
			_properties = new Properties();
			_properties.load(is);
			is.close();

			_log.info("Configuration file './" + configFileName + ".properties' loaded");
		}

		// Search for configuration file in all class paths
		// -----------------------------------------------------------------
		if (_properties == null) {
			for (String path : getConfigPaths()) {
				file = new File(path + "/" + configFileName + ".properties");
				if (file.exists()) {
					FileInputStream is = new FileInputStream(file);

					// Create properties obj;
					_properties = new Properties();
					_properties.load(is);
					is.close();

					_log.info("Configuration file '" + path + "/" + configFileName + ".properties' loaded");
					break;
				}
			}
		}

		// Read internal property file
		// -----------------------------------------------------------------
		if (_properties == null) {
			InputStream is = this.getClass().getClassLoader()
					.getResourceAsStream("properties/" + configFileName + ".properties");

			if (is != null) {
				// Create properties obj;
				_properties = new Properties();
				_properties.load(is);
				is.close();
			}

			_log.info("Configuration file INTERNAL '" + configFileName + ".properties' loaded");
		}

		// Override properties file with ENV variables if available
		// -----------------------------------------------------------------
		overrideWithEnvVars();
		
		// If _properties is still null I throw an expection
		// -----------------------------------------------------------------
		if (_properties == null) {
			throw new IOException("Impossible to find configuration file '" + configFileName + ".properties'");
		}

	}

	private	void overrideWithEnvVars(){
		boolean hasChanged = false;
		
	    @SuppressWarnings("rawtypes")
		Enumeration e = _properties.propertyNames();
	    while (e.hasMoreElements()) {
	    	String key = (String) e.nextElement();
	    	String expectedVarName = "DigiHome_" + _configFileName + "_" + key;
	    	if(System.getenv(expectedVarName) != null && !System.getenv(expectedVarName).isEmpty()) {
	    		System.out.println("Overriding " + key + " with environemnt: " + System.getenv(expectedVarName));
	    		_properties.setProperty(key, System.getenv(expectedVarName));
	    		hasChanged = true;
	    	}
	    }
	    
	    if(hasChanged){
	    	System.out.println("New values after override");
	    	
	    	@SuppressWarnings("rawtypes")
			Enumeration a = _properties.propertyNames();
		    while (a.hasMoreElements()) {
		    	String key = (String) a.nextElement();
		    	System.out.println(key + " = " + _properties.getProperty(key));
		    }	    	
	    }
	}

	public Properties getProperties() {
		return _properties;
	}

	/**
	 * Provides all classpath.
	 *
	 * @return
	 */
	private static String[] getConfigPaths() {
		String classpath = System.getProperty("java.class.path");

		return classpath.split(File.pathSeparator);
	}

}
