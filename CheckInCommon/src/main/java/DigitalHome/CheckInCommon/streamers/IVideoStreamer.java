package DigitalHome.CheckInCommon.streamers;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Map.Entry;

public interface IVideoStreamer extends Runnable {

	BufferedImage getCurrentImage();

	BufferedImage getCurrentImageWithDetection();

	BufferedImage getDetectedFace(int squareSideSize);

	ArrayList<Entry<BufferedImage, Double>> getAllDetectedFaces(int squareSideSize);

	void stop();

	void run();

	int getCaptureWidth();

	int getCaptureHeight();
	
	boolean isRunning();

}