package DigitalHome.CheckInCommon.streamers;

import static org.bytedeco.javacpp.opencv_imgproc.COLOR_RGB2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.INTER_CUBIC;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.equalizeHist;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.AbstractMap.SimpleEntry;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;
import static org.bytedeco.javacpp.opencv_objdetect.CASCADE_SCALE_IMAGE;

import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;

import DigitalHome.CheckInCommon.utilities.Toolkit;

public class IPCamVideoStreamer implements IVideoStreamer {

	// Number of grabbed frames to skip
	private int framesToSkip;

	// Source for capture informations
	private int captureWidth;
	private int captureHeight;

	private Size minDetectedFaceSize;
	private Size maxDetectedFaceSize;

	// Correction flags
	private boolean correctEqualizeHist = true;

	// Cascade file for detection
	final private static String CASCADE_FILE = "haarcascade_frontalface_default.xml";

	// Other internal objects for frame
	private FFmpegFrameGrabber grabber;	
	private String camUrl;
	private OpenCVFrameConverter.ToMat converterToMat;

	// Other internal objects for face detection
	private CascadeClassifier face_cascade;

	// Flag used to end execution
	private boolean running;
	private boolean shouldRun;

	// Mat representing the current image grabbed
	private Mat currentFrameAsMat;

	/**
	 * Class constructor
	 * @throws IOException 
	 */	
	public IPCamVideoStreamer(String streamerIPCAMBonjourName, String streamerIPCAMAddress, int streamerStreamFormat, int captureWidth,
			int captureHeight, boolean correctEqualizeHist, int minCaptureSize, int framesToSkip) throws IOException 
	{

		this.minDetectedFaceSize = new Size(minCaptureSize, minCaptureSize);
		this.maxDetectedFaceSize = new Size(2048, 2048);
		
		this.captureWidth = captureWidth;
		this.captureHeight = captureHeight;
		this.correctEqualizeHist = correctEqualizeHist;
		this.framesToSkip = framesToSkip;
		
		// Get the URL via Bonjour recognition
		String IPAddress;
		if (streamerIPCAMAddress == null || streamerIPCAMAddress.trim().isEmpty()) {
			IPAddress = Toolkit.getIPFromBonjourName(streamerIPCAMBonjourName);
		} else {
			IPAddress = streamerIPCAMAddress;
		}
		
		// Get the complete URL
		this.camUrl = "rtsp://admin:admin@" + IPAddress + ":554/play" + streamerStreamFormat + ".sdp";
		System.out.println("Start reading video stream from IPCAM: " + this.camUrl);

		// The available FrameGrabber classes include OpenCVFrameGrabber (opencv_videoio),
		// DC1394FrameGrabber, FlyCaptureFrameGrabber, OpenKinectFrameGrabber,
		// PS3EyeFrameGrabber, VideoInputFrameGrabber, and FFmpegFrameGrabber.
		grabber = new FFmpegFrameGrabber(camUrl);
		grabber.setAudioChannels(0);
		grabber.setFormat("rtsp");
		grabber.setImageWidth(captureWidth);
		grabber.setImageHeight(captureHeight);			
		
		// Create the converter to convert captured Frames to MAT
		converterToMat = new OpenCVFrameConverter.ToMat();

		// Copy the classifier to a temporary folder
		String tempSuffix = "tmpClass";
		Path tmp = Files.createTempFile(tempSuffix, ".xml");
		Files.copy(this.getClass().getClassLoader().getResourceAsStream("classifiers/" + CASCADE_FILE), tmp,
				StandardCopyOption.REPLACE_EXISTING);

		// Get the face cascade classifier (I remove the first char because is a
		// / and creates problems
		face_cascade = new CascadeClassifier(tmp.toString());

		// Set currentFrame to null to start
		currentFrameAsMat = null;

		// Set that the streamer is not running
		shouldRun = false;
		running = false;
	}

	private synchronized Mat getCurrentFrameAsMat() {
		if (currentFrameAsMat != null) {
			return currentFrameAsMat.clone();
		} else {
			return null;
		}
	}	

	private synchronized void setCurrentFrameAsMat(Mat frameAsMat) {
		if (currentFrameAsMat != null) {
			currentFrameAsMat.release();
		}
		
		currentFrameAsMat = frameAsMat.clone();
	}

	private BufferedImage Mat2BufferedImage(Mat m) {
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.data().get(b); // get all the pixels

		BufferedImage img = new BufferedImage(m.cols(), m.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return img;
	}

	private synchronized RectVector detectAll(Mat src) {

		// If the source received is empty I return null
		if (src.empty()) {
			return null;
		}

		// Transform the image to Gray for detection
		Mat correctedImg = new Mat();
		cvtColor(src, correctedImg, COLOR_RGB2GRAY);

		// Apply transformations if required
		if (correctEqualizeHist)
			equalizeHist(correctedImg, correctedImg);

		// If the output remains null it means that the image was just a shade
		if (correctedImg.empty()) {
			correctedImg.release();
			return null;
		}

		// If the image is valid, I try to detect
		RectVector faces = new RectVector();
		face_cascade.detectMultiScale(correctedImg, faces, 1.05, 5, 0 | CASCADE_SCALE_IMAGE, 
				minDetectedFaceSize, maxDetectedFaceSize);

		// Release the temporary MAT file
		correctedImg.release();

		// Return output
		return faces;
	}

	private Rect detectMainFace(Mat src) {

		// Detect all faces
		RectVector faces = detectAll(src);
		if(faces == null) {
			return null;
		}
		
		// Get the position of all the faces
		// search the "best" face as most central and big.
		Rect currentBestFace = null;
		long currentBestFacePoints = 0;

		for (int i = 0; i < faces.size(); i++) {
			Rect face_i = faces.get(i);

			long points = face_i.width() * face_i.height();

			if (currentBestFacePoints < points) {
				currentBestFace = face_i;
				currentBestFacePoints = points;
			}
		}

		// Write the rectangle over the original image
		return currentBestFace;
	}

	/*
	 * (non-Javadoc)
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#getCurrentImage()
	 */
	public BufferedImage getCurrentImage() {
		BufferedImage output = null;
		Mat tempCurrentFrame = getCurrentFrameAsMat();
		if (tempCurrentFrame == null) {
			System.out.println("No image found from streamer");
			return null;
		}

		// Convert current frame to BufferedImage
		output = Mat2BufferedImage(tempCurrentFrame);

		// Release tempCurrentFrame to avoid memory leaks
		tempCurrentFrame.release();

		// return the buffered image
		return output;
	}

	/*
	 * (non-Javadoc)
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#getDetectedImage()
	 */
	public BufferedImage getCurrentImageWithDetection() {
		BufferedImage output = null;

		Mat tempCurrentFrame = getCurrentFrameAsMat();
		if (tempCurrentFrame == null) {
			System.out.println("No image found from streamer");
			return null;
		}

		// Detect the best face in the image and if detected draw a rectangle
		// around it
		Rect detectedFaceRect = detectMainFace(tempCurrentFrame);
		if (detectedFaceRect != null)
			rectangle(tempCurrentFrame, detectedFaceRect, new Scalar(0, 255, 0, 1));

		// Convert current frame to BufferedImage
		output = Mat2BufferedImage(tempCurrentFrame);

		// Release tempCurrentFrame to avoid memory leaks
		tempCurrentFrame.release();

		// return the buffered image
		return output;
	}

	/*
	 * (non-Javadoc)
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#getDetectedFace(int)
	 */
	public BufferedImage getDetectedFace(int squareSideSize) {
		BufferedImage detectedFace = null;

		// Try to detect a face
		Mat tempCurrentFrame = getCurrentFrameAsMat();
		if (tempCurrentFrame == null) {
			System.out.println("No image found from streamer");				

		} else {
			
			Rect detectedFaceRect = detectMainFace(tempCurrentFrame);
			if (detectedFaceRect != null) {
				Mat detectedFaceMat = new Mat(tempCurrentFrame, detectedFaceRect);
				resize(detectedFaceMat, detectedFaceMat, new Size(squareSideSize, squareSideSize), 0, 0, INTER_CUBIC);
				detectedFace = Mat2BufferedImage(detectedFaceMat);
				detectedFaceMat.release();
			}

			// Release tempCurrentFrame to avoid memory leaks			
			tempCurrentFrame.release();
		}

		return detectedFace;
	}

	/* (non-Javadoc)
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#getAllDetectedFaces(int)
	 */
	public ArrayList<Entry<BufferedImage, Double>> getAllDetectedFaces(int squareSideSize) {

		ArrayList<Entry<BufferedImage, Double>> detectedFacesArray = new ArrayList<Entry<BufferedImage, Double>>();

		// Get current frame, if frame is null continue
		Mat tempCurrentFrame = getCurrentFrameAsMat();
		if (tempCurrentFrame == null) {
			System.out.println("No image found from streamer");
		} else {
		
			// Detect all faces and get rectangles representing their
			// position
			RectVector detectedFaceRects = detectAll(tempCurrentFrame);
			
			if (detectedFaceRects != null) {

				// For each rectangle calculate score, cut the current
				// frame, resize, convert to image and add to output
				for (int rectIndex = 0; rectIndex < detectedFaceRects.size(); rectIndex++) {
					Rect currRect = detectedFaceRects.get(rectIndex);

					// Calculate the distance between Rect center and image
					// center using Pitagora.
					// Note: Rect X and Y are the coordinates of top-left
					// corner
					// calculated from the bottom left corner of the
					// original image
					Double distanceFromCenter = Math.sqrt(Math
							.pow(((double) currRect.x() + (currRect.width() / 2)) - (tempCurrentFrame.cols() / 2), 2)
							+ Math.pow(
									((double) currRect.y() - (currRect.height() / 2)) - (tempCurrentFrame.rows() / 2),
									2));

					// Calculate the length of the diagonal (distance of
					// corner from center)
					Double imageDiagonaHalflLenght = Math
							.sqrt(Math.pow(tempCurrentFrame.cols() / 2, 2) + Math.pow(tempCurrentFrame.rows() / 2, 2));

					// Get the dimension of the rectangle
					Double rectDimension = (double) currRect.width() * currRect.height();

					// Calculate score
					Double currScore = rectDimension * (1 - (distanceFromCenter / imageDiagonaHalflLenght));

					// Get the image and resize it
					Mat detectedFaceMat = new Mat(tempCurrentFrame, currRect);
					resize(detectedFaceMat, detectedFaceMat, new Size(squareSideSize, squareSideSize), 0, 0,
							INTER_CUBIC);

					// Add image with score to HashMap
					detectedFacesArray
							.add(new SimpleEntry<BufferedImage, Double>(Mat2BufferedImage(detectedFaceMat), currScore));
					detectedFaceMat.release();
				}
			}

			// Release tempCurrentFrame to avoid memory leaks
			tempCurrentFrame.release();				
		}

		return detectedFacesArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#stop()
	 */
	public void stop() {
		shouldRun = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#run().
	 */
	public void run() {

		Frame capturedFrame;
		int skippedFramesCount;

		// This tells to the streamer that it should go on running.
		shouldRun = true;
		
		while (shouldRun) {
			
			// Start the stream
			try {				
				System.out.println("Starting the stream");
				grabber.start();
				running = true;
				System.out.println("Stream started");
			} catch (Exception e) {
				System.err.println("Impossible to start IP Cam video grabber");
				e.printStackTrace();
			}
			
			// skippedFramesCount is used to skip some frame and avoid system OverCharge			
			skippedFramesCount = 0;
			try {
				
				// Cycle grabbing images
				while (shouldRun && (capturedFrame = grabber.grabImage()) != null) {
	
					// Continue if image is null
					if(capturedFrame.image == null){
						System.out.println("Empty image grabbed - Skipping");
						continue;
					}				
	
					// If i didn't reach the max number of skipped, I continue grabbing
					skippedFramesCount ++;
					if (skippedFramesCount <= framesToSkip) {
						continue;
					}
					
					// Reset counter
					skippedFramesCount = 0;
					
					// Crop image and save it
					Mat tmpFrameAsMat = converterToMat.convert(capturedFrame);
					setCurrentFrameAsMat(tmpFrameAsMat);
					tmpFrameAsMat.release();
					
					// Flush queued packets to free up the queue
					// grabber.flush();
				}
			} catch (Exception e) {
				System.out.println("Frame grabber Exception - Exiting");
				e.printStackTrace();
			}

			// Mark the grabber as NOT RUNNING
			running = false;			
			
			// If I'm here it means that the stream has fallen
			// Clean an restart the Stream
			try {
				System.out.println("Stopping the grabber");				
				grabber.flush();
				grabber.stop();
			} catch (Exception e) {
				System.err.println("Error stopping grabber");
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see DigitalHome.CheckInCommon.streamers.IVideoStreamer#getCaptureWidth()
	 */
	public int getCaptureWidth() {
		return captureWidth;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * DigitalHome.CheckInCommon.streamers.IVideoStreamer#getCaptureHeight()
	 */
	public int getCaptureHeight() {
		return captureHeight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * DigitalHome.CheckInCommon.streamers.IVideoStreamer#isRunning()
	 */
	public boolean isRunning() {
		return running;
	}	
	
}