package DigitalHome.CheckInCommon.transport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.codehaus.jackson.annotate.JsonProperty;

public class ResponseRecognize {
	private String status;
	private String message;
	private ArrayList<ResponseRecognizeScore> results = new ArrayList<ResponseRecognizeScore>();

	public ResponseRecognize(){
	}
	
	public ResponseRecognize(String status, String message, Collection<ResponseRecognizeScore> results){
		this.setResults(results);
		this.setStatus(status);
	}
	
	public void add(ResponseRecognizeScore result){
		this.getResults().add(result);
		Collections.sort(this.results);
		Collections.reverse(this.results);		
	}

	public void addAll(Collection<ResponseRecognizeScore> results) {
		this.getResults().addAll(results);
		Collections.sort(this.results);
		Collections.reverse(this.results);
	}	
	
	public ResponseRecognizeScore remove(int index){
		return this.getResults().remove(index);
	}

	@JsonProperty("status")	
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")	
	private void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("message")		
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")		
	public void setMessage(String message) {
		this.message = message;
	}	
	
	@JsonProperty("results")
	public ArrayList<ResponseRecognizeScore> getResults() {
		return results;
	}	

	@JsonProperty("results")	
	private  void setResults(Collection<ResponseRecognizeScore> images) {
		this.results.clear();
		this.results.addAll(images);
		Collections.sort(this.results);
		Collections.reverse(this.results);
	}	
	
	public ArrayList<ResponseRecognizeScore> getResults(Double minPercentage) {

		ArrayList<ResponseRecognizeScore> output = new ArrayList<ResponseRecognizeScore>();
		
		// No user has been recognized
		if(results.size() == 0) {
			System.out.println("The recognization didn't identify any user");
			return output;
		}
		
		// Add all results with a percentage of confidence greater than required
		for(ResponseRecognizeScore currResult : results) {
			if(currResult.getScore() >= minPercentage) {
				output.add(currResult);
			} else {
				// The collection is ordered so I can exit
				break;
			}
		}
		
		// If I have some recognized users but no one satisfy the confidence, then I write it	
		if(output.size() == 0) {
			System.out.println("No user found with the required percentage. Best match was " + results.get(0).getId() + " with score " + results.get(0).getScore() + " but we required a minimum of " + minPercentage);
		} else {
			System.out.println("YES!! Selected " + output.size() + " possible users with a maximum confidence of " + output.get(0).getScore() + " and a minimum of " + output.get(output.size() - 1).getScore());
		}

		return output;
	}
}
