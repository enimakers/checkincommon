package DigitalHome.CheckInCommon.transport;

import org.codehaus.jackson.annotate.JsonProperty;

public class GalleryImage {
	private String image_id;
	private String image_b64;
	
	public GalleryImage(String image_id, String image_b64){
		this.setImage_id(image_id);
		this.setImage_b64(image_b64);	
	}

	@JsonProperty("image_id")	
	public String getImage_id() {
		return image_id;
	}
	
	@JsonProperty("image_id")	
	private void setImage_id(String image_id) {
		this.image_id = image_id;
	}
	
	@JsonProperty("image_b64")	
	public String getImage_b64() {
		return image_b64;
	}

	@JsonProperty("image_b64")	
	public void setImage_b64(String bs) {
		this.image_b64 = bs;
	}
}