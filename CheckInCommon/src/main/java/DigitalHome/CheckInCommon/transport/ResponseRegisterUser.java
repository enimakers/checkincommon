package DigitalHome.CheckInCommon.transport;

import org.codehaus.jackson.annotate.JsonProperty;

public class ResponseRegisterUser {
	private String message;

	public ResponseRegisterUser(){ }	
	
	@JsonProperty("message")	
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")	
	public void setMessage(String message) {
		this.message = message;
	}
	
}
