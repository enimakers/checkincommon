package DigitalHome.CheckInCommon.transport;

import org.codehaus.jackson.annotate.JsonProperty;

public class ResponseRecognizeScore implements Comparable<ResponseRecognizeScore>{
	private Double score;
	private String id;

	public ResponseRecognizeScore(){
	}	
	
	public ResponseRecognizeScore(Double score, String id){
		this.setScore(score);
		this.setId(id);
	}
	
	@JsonProperty("score")	
	public Double getScore() {
		return score;
	}
	
	@JsonProperty("score")	
	private void setScore(Double score) {
		this.score = score;
	}
	
	@JsonProperty("id")	
	public String getId() {
		return id;
	}
	
	@JsonProperty("id")	
	private void setId(String id) {
		this.id = id;
	}
	
	public int compareTo(ResponseRecognizeScore compareRespScore) {

		// Compare the two double
		Double compareScore = ((ResponseRecognizeScore) compareRespScore).getScore();
		return this.getScore().compareTo(compareScore);
	}
}
