package DigitalHome.CheckInCommon.transport;

import java.util.ArrayList;
import java.util.Collection;

import org.codehaus.jackson.annotate.JsonProperty;

public class Gallery {
	private ArrayList<GalleryImage> images;
	
	public Gallery(Collection<GalleryImage> images){
		this.setImages(new ArrayList<GalleryImage>());
		this.getImages().addAll(images);
	}	
	
	public Gallery(){
		this.setImages(new ArrayList<GalleryImage>());
	}
	
	public void add(GalleryImage img){
		this.getImages().add(img);
	}

	public void addAll(Collection<GalleryImage> images) {
		this.getImages().addAll(images);
	}	
	
	public GalleryImage remove(int index){
		return this.getImages().remove(index);
	}

	@JsonProperty("images")	
	public ArrayList<GalleryImage> getImages() {
		return images;
	}	

	@JsonProperty("images")		
	private void setImages(ArrayList<GalleryImage> images) {
		this.images = images;
	}	
}
