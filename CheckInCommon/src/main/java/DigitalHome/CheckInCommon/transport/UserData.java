package DigitalHome.CheckInCommon.transport;

import java.util.ArrayList;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonProperty;

public class UserData{
	private String id;
	private String name;
	private String pref_color;
	private Double pref_temperature;
	private String pref_music;
	private ArrayList<String> images_b64;
	
	public UserData(){
		
	}
	
	public UserData(UUID id, String name, String pref_color, Double pref_temperature,String pref_music, ArrayList<String> images_b64){
		this(id.toString(), name, pref_color, pref_temperature, pref_music, images_b64);
	}
	
	public UserData(String id, String name, String pref_color, Double pref_temperature, String pref_music, ArrayList<String> images_b64){
		this.setId(id);
		this.setName(name);
		this.setPref_color(pref_color);
		this.setPref_temperature(pref_temperature);
		this.setPref_music(pref_music);
		this.setImages_b64(images_b64);		
	}		

	@JsonProperty("pref_temperature")
	public Double getPref_temperature() {
		return pref_temperature;
	}

	@JsonProperty("pref_temperature")
	public void setPref_temperature(Double pref_temperature) {
		this.pref_temperature = pref_temperature;
	}

	@JsonProperty("name")	
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("pref_color")
	public String getPref_color() {
		return pref_color;
	}

	@JsonProperty("pref_color")	
	public void setPref_color(String pref_color) {
		this.pref_color = pref_color;
	}

	@JsonProperty("pref_music")	
	public String getPref_music() {
		return pref_music;
	}

	@JsonProperty("pref_music")
	public void setPref_music(String pref_music) {
		this.pref_music = pref_music;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")	
	public void setId(String id) {
		this.id = id.toString();
	}

	@JsonProperty("images_b64")	
	public ArrayList<String> getImages_b64() {
		return images_b64;
	}
	
	@JsonProperty("images_b64")	
	public void setImages_b64(ArrayList<String> images_b64) {
		this.images_b64 = new ArrayList<String>();
		if (images_b64 != null) {
			this.images_b64.addAll(images_b64);
		}
	}
}