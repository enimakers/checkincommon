package DigitalHome.CheckInCommon.transport;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.codehaus.jackson.annotate.JsonProperty;

import DigitalHome.CheckInCommon.utilities.Toolkit;

public class DetectedImage {
	private String image;

	public DetectedImage(BufferedImage image_buff) throws IOException{
		this(Toolkit.bufferedImageToBase64(image_buff));
	}
	
	public DetectedImage(String image){
		this.setImage(image);
	}

	@JsonProperty("image")	
	public String getImage() {
		return image;
	}		

	@JsonProperty("image")	
	private void setImage(String image) {
		this.image = image;
	}
}