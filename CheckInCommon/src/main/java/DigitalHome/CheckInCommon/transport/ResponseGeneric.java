package DigitalHome.CheckInCommon.transport;

import org.codehaus.jackson.annotate.JsonProperty;

public class ResponseGeneric {
	private String status;
	private String message;
	
	public ResponseGeneric(){	
	}
	
	@JsonProperty("status")	
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")		
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("message")		
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")	
	public void setMessage(String message) {
		this.message = message;
	}
}
