package DigitalHome.CheckInCommon.databaseClient;

import java.io.IOException;
import java.util.UUID;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import DigitalHome.CheckInCommon.configure.Configuration;
import DigitalHome.CheckInCommon.transport.UserData;

public class CassandraClient {
	
	// Configuration file name for this class
	private static final String CONFIG_FILE_NAME = "CassandraClient";	
	
	private String _node1;
	private String _node2;
	private String _userName;
	private String _userPassword;
	
	private Cluster _cluster;
	private Session _session;
	
	public Session getSession() {
		return this._session;
	}

    private void configure() throws IOException{
    	
		// Get the configurator
		Configuration config = new Configuration(CONFIG_FILE_NAME);

		// Cluster nodes
		_node1 = config.getProperties().getProperty("node1ServerName");
		_node2 = config.getProperties().getProperty("node2ServerName");

		_userName = config.getProperties().getProperty("userName");
		_userPassword = config.getProperties().getProperty("userPassword");
    }		
	
	public CassandraClient() throws IOException {
		
		//Configure from configuration file
		configure();
		
		Cluster.Builder clusterBuilder = Cluster.builder();
		if (_node1 != null && !_node1.isEmpty()) clusterBuilder.addContactPoint(_node1);
		if (_node2 != null && !_node2.isEmpty()) clusterBuilder.addContactPoint(_node2);
		
		clusterBuilder.withCredentials(_userName, _userPassword);

		_cluster = clusterBuilder.build();
	
		Metadata metadata = _cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for ( Host host : metadata.getAllHosts() ) {
			System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
		}
		
		connect();
	}
	
	public void connect() {
		if (_session == null || _session.isClosed()) {
			_session = _cluster.connect();
		}
	}
	
	public void createSchema() throws Exception {
		Session currentSession = getSession();
		if(_session == null || currentSession.isClosed()){
			throw new Exception("Connection to cassandra is closed");
		}
		
		currentSession.execute(
		            "CREATE TABLE IF NOT EXISTS digitalhome.users_data (" +
		            	"id uuid PRIMARY KEY," + 
		            	"name text," + 
		            	"pref_color text," + 
		            	"pref_temperature double," + 
		            	"pref_music text" + 
					");");
	}

	public void saveUserData(UserData userData) throws Exception {
		Session currentSession = getSession();
		if(_session == null || currentSession.isClosed()){
			throw new Exception("Connection to cassandra is closed");
		}
		
		PreparedStatement statement = currentSession.prepare(
					"INSERT INTO digitalhome.users_data " +
					"       (id, name, pref_color, pref_temperature, pref_music) " +
					"VALUES (?, ?, ?, ?, ?);");
		
		BoundStatement boundStatement = new BoundStatement(statement);
		currentSession.execute(boundStatement.bind(
					UUID.fromString(userData.getId()), 
					userData.getName(), 
					userData.getPref_color(), 
					userData.getPref_temperature(), 
					userData.getPref_music() ) );
	}

	public UserData getUserDataByID(String userId) throws Exception {
		UserData output = null;
		Row result;
		
		Session currentSession = getSession();
		if(_session == null || currentSession.isClosed()){
			throw new Exception("Connection to cassandra is closed");
		}		
		
		PreparedStatement statement = currentSession.prepare(
					"SELECT * FROM digitalhome.users_data " +
					"WHERE id = ?;");
	
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet results = currentSession.execute(boundStatement.bind(
					UUID.fromString(userId) ) );
		
		// Get only one row because the query can return one result only
		if ((result = results.one()) != null){
			output = new UserData(
					result.getUUID("id"),
					result.getString("name"),
					result.getString("pref_color"),
					result.getDouble("pref_temperature"),
					result.getString("pref_music"),
					null);
		}
			
		return output;
	}

	public void close() {
		_session.close();
		_cluster.close();
	}
}
