package DigitalHome.CheckInCommon.restClient;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import DigitalHome.CheckInCommon.configure.Configuration;

public class HomeGatewayClient {

	// Configuration file name for this class
	private static final String CONFIG_FILE_NAME = "HomeGatewayClient";
	
	// Properties loaded from CONFIG file
	private String endPointURL;
	private String utenteNomeIDX;
	private String temperatureIDX;
	private String resetIDX;
	private HashMap<String, String> lightIDX;

	// private objects for internal use
    private Client client;

    private void configure() throws IOException{
		// Get the configurator
		Configuration config = new Configuration(CONFIG_FILE_NAME);
		
		// Get end point from configuration file
		endPointURL = config.getProperties().getProperty("endPointURL");

		//Get nome utente IDX from configuration file 
		utenteNomeIDX = config.getProperties().getProperty("utenteNomeIDX");
		
		// Get temperature IDX from configuration file
		temperatureIDX = config.getProperties().getProperty("temperatureIDX");		
		
		// Add all lights from configuration file
		lightIDX = new HashMap<String, String>();		
		lightIDX.put( "WHITE", config.getProperties().getProperty("whiteLightIDX"));
		lightIDX.put( "GREEN", config.getProperties().getProperty("greenLightIDX"));
		lightIDX.put( "BLUE", config.getProperties().getProperty("blueLightIDX"));
		lightIDX.put( "YELLOW", config.getProperties().getProperty("yellowLightIDX"));
		lightIDX.put( "OFF", config.getProperties().getProperty("lightOffIDX"));
		
		// Reset IDX
		resetIDX = config.getProperties().getProperty("resetIDX");			
    }
    
	public HomeGatewayClient() throws IOException {

		// Configure from cnofiguration file
		configure();

        // Create Jersey client
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        client = Client.create(clientConfig);
    }
	
	
	@SuppressWarnings("unused")
	private <T> T getRequest(Class<T> clazz, String path) 
			throws Exception {
        return this.getRequest(clazz, path, null);
	}
	
	private <T> T getRequest(Class<T> clazz, String path, MultivaluedMap<String, String> queryParams) 
			throws Exception {
		
        WebResource webResource = client.resource(endPointURL + "/" + path);
        webResource.accept(MediaType.APPLICATION_JSON_TYPE);

        if(queryParams != null) webResource = webResource.queryParams(queryParams);
        
        ClientResponse response = webResource.get(ClientResponse.class);
        if (response.getStatus() != 200) throw new WebApplicationException();
        
        T responseEntity = response.getEntity(clazz);
        
        return responseEntity;
	}
	
	public void setUserName(String userName) throws Exception {
		System.out.println("Setting user name: " + userName);
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		
		queryParams.add("type", "command");
		queryParams.add("param", "udevice");
		queryParams.add("idx", utenteNomeIDX);
		queryParams.add("nvalue", "0");
		queryParams.add("svalue", userName);
		
		getRequest(String.class, "json.htm", queryParams);
	}
	
	public void setTemperature(double dblTermometer) throws Exception {
		System.out.println("Setting temperature: " + dblTermometer);
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		
		queryParams.add("type", "command");
		queryParams.add("param", "udevice");
		queryParams.add("idx", temperatureIDX);
		queryParams.add("nvalue", "0");
		queryParams.add("svalue", String.valueOf(dblTermometer));
		
		getRequest(String.class, "json.htm", queryParams);
	}
	
	public void setLight(String color) throws Exception {
		System.out.println("Setting color: " + color);
		
		// Check if the color received is invalid
		if(!lightIDX.containsKey(color)) throw new Exception("Invalid color " + color);
		
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		
		queryParams.add("type", "command");
		queryParams.add("param", "switchscene");
		queryParams.add("idx", lightIDX.get(color));
		queryParams.add("switchcmd", "On");
		
		getRequest(String.class, "json.htm", queryParams);
	}	
	
	public void checkOut() throws Exception {
		System.out.println("Checking out");
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
				
		queryParams.add("type", "command");
		queryParams.add("param", "switchlight");
		queryParams.add("idx", resetIDX);
		queryParams.add("switchcmd", "On");
		
		getRequest(String.class, "json.htm", queryParams);
	}		
}
