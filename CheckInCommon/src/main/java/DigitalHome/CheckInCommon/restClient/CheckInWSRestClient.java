package DigitalHome.CheckInCommon.restClient;

import java.io.IOException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import DigitalHome.CheckInCommon.configure.Configuration;
import DigitalHome.CheckInCommon.transport.ResponseRegisterUser;
import DigitalHome.CheckInCommon.transport.UserData;

public class CheckInWSRestClient {

	// Configuration file name for this class
	private static final String CONFIG_FILE_NAME = "CheckInWSRestClient";	
	
	private String _endPointURL;
    private Client _client;
    	
    private void configure() throws IOException{
    	
		// Get the configurator
		Configuration config = new Configuration(CONFIG_FILE_NAME);
		
		_endPointURL = config.getProperties().getProperty("endPointURL");
    }    
    
	public CheckInWSRestClient() throws IOException {
		
		// Configure from configuration file
		configure();
		
        // Create Jersey client		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		_client = Client.create(clientConfig);
	}
	
	@SuppressWarnings("unused")
	private <T> T getRequest(Class<T> clazz, String path) {
        return this.getRequest(clazz, path, null);
	}
	
	private <T> T getRequest(Class<T> clazz, String path, MultivaluedMap<String, String> queryParams) {
		
        WebResource webResource = _client.resource(_endPointURL + "/" + path);
        webResource.accept(MediaType.APPLICATION_JSON_TYPE);
        
        if(queryParams != null) webResource = webResource.queryParams(queryParams);
        
        ClientResponse response = webResource.get(ClientResponse.class);
        if (response.getStatus() != 200) throw new WebApplicationException();
        
        T responseEntity = response.getEntity(clazz);

        return responseEntity;
	}
	
	private <T> T postRequest(Class<T> clazz, String path, Object postPayload) {
        return this.postRequest(clazz, path, postPayload, null);
	}
	
	private <T> T postRequest(Class<T> clazz, String path, Object postPayload, MultivaluedMap<String, String> queryParams) {
	
        WebResource webResource = _client.resource(_endPointURL + "/" + path);
        webResource.accept(MediaType.APPLICATION_JSON_TYPE);
        
        if(queryParams != null) webResource = webResource.queryParams(queryParams);
        
        ClientResponse response = webResource.type("application/json").post(ClientResponse.class, postPayload);
        if (response.getStatus() != 200) throw new WebApplicationException();
        
        T responseEntity = response.getEntity(clazz);

        return responseEntity;
	}
	
	public void registerUser(UserData payload){
		try {
			ResponseRegisterUser resp = postRequest(ResponseRegisterUser.class, "registerUser", payload);
			if(!resp.getMessage().equals("ok")) throw new WebApplicationException();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}
