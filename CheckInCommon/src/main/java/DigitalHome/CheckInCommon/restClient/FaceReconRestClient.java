package DigitalHome.CheckInCommon.restClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import DigitalHome.CheckInCommon.configure.Configuration;
import DigitalHome.CheckInCommon.transport.DetectedImage;
import DigitalHome.CheckInCommon.transport.Gallery;
import DigitalHome.CheckInCommon.transport.GalleryImage;
import DigitalHome.CheckInCommon.transport.ResponseGeneric;
import DigitalHome.CheckInCommon.transport.ResponseRecognize;

import java.nio.charset.StandardCharsets;

public class FaceReconRestClient {

	// Configuration file name for this class
	private static final String CONFIG_FILE_NAME = "FaceReconRestClient";	
	
	private String _endPointURL;
	private String _galleryName;
    private Client _client;

    private void configure() throws IOException{
    	
		// Get the configurator
		Configuration config = new Configuration(CONFIG_FILE_NAME);
		
		_endPointURL = config.getProperties().getProperty("endPointURL");
		_galleryName = config.getProperties().getProperty("galleryName");
    }      
    
	public FaceReconRestClient() throws IOException {

		// Configure from configuration file
		configure();
		
        // Create Jersey client
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        _client = Client.create(clientConfig);
    }
	
	@SuppressWarnings("unused")
	private <T> T getRequest(Class<T> clazz, String path) {
        return this.getRequest(clazz, path, null);
	}
	
	private <T> T getRequest(Class<T> clazz, String path, MultivaluedMap<String, String> queryParams) throws WebApplicationException {

        WebResource webResource = _client.resource(_endPointURL + "/" + path);
        webResource.accept(MediaType.APPLICATION_JSON_TYPE);
        
        if(queryParams != null) webResource = webResource.queryParams(queryParams);
        
        ClientResponse response = webResource.get(ClientResponse.class);
        if (response.getStatus() != 200) throw new WebApplicationException();
        
        T responseEntity = response.getEntity(clazz);
        
        return responseEntity;
	}
	
	private <T> T postRequest(Class<T> clazz, String path, Object postPayload) {
        return this.postRequest(clazz, path, postPayload, null);
	}
	
	private <T> T postRequest(Class<T> clazz, String path, Object postPayload, MultivaluedMap<String, String> queryParams) throws WebApplicationException {

        WebResource webResource = _client.resource(_endPointURL + "/" + path);
        webResource.accept(MediaType.APPLICATION_JSON_TYPE);
        
        if(queryParams != null) webResource = webResource.queryParams(queryParams);
        
        ClientResponse response = webResource.type("application/json").post(ClientResponse.class, postPayload);
        if (response.getStatus() != 200) throw new WebApplicationException();
        
        T responseEntity = response.getEntity(clazz);
        
        return responseEntity;
	}	
	
	public void addImagesToGallery(String ID, ArrayList<String> images_b64) throws Exception{
		ResponseGeneric response = null;
		
    	// Convert images to GalleryImages
    	ArrayList<GalleryImage> galleryImages = new ArrayList<GalleryImage>(); 
    	for(String currentImage_b64 : images_b64){
    		galleryImages.add(new GalleryImage(ID, currentImage_b64));
    	}
    	
    	// Create the gallery for POST call payload
		Gallery payload = new Gallery(galleryImages);
		
		// Call the service
		try {
			response = postRequest(ResponseGeneric.class, URLEncoder.encode(_galleryName, StandardCharsets.UTF_8.toString()) + "/images", payload);
			
			if (response == null) {
				throw new Exception("Recon error: unexpected");
			} else if (!response.getStatus().equals("ok")) {
				throw new Exception("Impossible to add images to gallery: " + response.getMessage());
			}
		} catch (UnsupportedEncodingException e) {
			throw new Exception("Error encoding for 'gallery name' sending data to WS", e);
		} catch (WebApplicationException e) {
			throw new Exception("Error connecting to face recognition server", e);
		}
	}	
	
	public ResponseRecognize recognizeDetectedImage(DetectedImage img) throws Exception{
		ResponseRecognize response = null;
		try {
			System.out.println("Recognizing image");
			response = postRequest(ResponseRecognize.class, URLEncoder.encode(_galleryName, StandardCharsets.UTF_8.toString()) + "/recognize", img);
			
			if (response == null) {
				throw new Exception("Recon error: unexpected");
			} else if (response.getStatus().equals("ko")){
				throw new Exception("Recon error: " + response.getMessage());
			}
			
		} catch (UnsupportedEncodingException e) {
			throw new Exception("Error encoding for 'gallery name' sending data to WS", e);			
		} catch (WebApplicationException e) {
			throw new Exception("Error connecting to face recognition server", e);
		}

		return response;
	}
	
}