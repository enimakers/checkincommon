package DigitalHome.CheckInCommon.utilities;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.Enumeration;

import javax.imageio.ImageIO;
import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

public class Toolkit {
	
	public static String bufferedImageToBase64(BufferedImage input) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(input, "jpg", baos );
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();		
		
		return Base64.getEncoder().encodeToString(imageInByte);
	}
	
	public static BufferedImage makeRoundedCorner(BufferedImage image, int cornerRadius) {
	    int w = image.getWidth();
	    int h = image.getHeight();
	    BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2 = output.createGraphics();

	    // This is what we want, but it only does hard-clipping, i.e. aliasing
	    // g2.setClip(new RoundRectangle2D ...)

	    // so instead fake soft-clipping by first drawing the desired clip shape
	    // in fully opaque white with anti-aliasing enabled...
	    g2.setComposite(AlphaComposite.Src);
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.setColor(Color.WHITE);
	    g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));

	    // ... then composing the image on top,
	    // using the white shape from above as alpha source
	    g2.setComposite(AlphaComposite.SrcAtop);
	    g2.drawImage(image, 0, 0, null);

	    g2.dispose();

	    return output;
	}
	
	public static String getIPFromBonjourName(String bonjourName) throws IOException{
		String IPAddresses [] = null;
		
		String bonjourServiceType = "_http._tcp.local.";

		ServiceListener bonjourServiceListener = new ServiceListener() {
		    public void serviceAdded(ServiceEvent event) {
		        event.getDNS().requestServiceInfo(event.getType(), event.getName(), true);
		    }

		    public void serviceRemoved(ServiceEvent event) {
		    }

		    public void serviceResolved(ServiceEvent event) {		    }
		};

	    Enumeration<NetworkInterface> ifc = NetworkInterface.getNetworkInterfaces();
	    while (ifc.hasMoreElements() && IPAddresses == null) {
	        NetworkInterface anInterface = ifc.nextElement();
	        if (anInterface.isUp()) {
	            Enumeration<InetAddress> addr = anInterface.getInetAddresses();
	            
	            while (addr.hasMoreElements() && IPAddresses == null) {
	                InetAddress address = addr.nextElement();
	                
	                final JmDNS jmdns = JmDNS.create(address, bonjourServiceType);
	                jmdns.addServiceListener(bonjourServiceType, bonjourServiceListener);              

	        		ServiceInfo[] serviceInfos = jmdns.list(bonjourServiceType);
	        		for (ServiceInfo info : serviceInfos) {
	        			if (info.getName().equals(bonjourName)) {
	        				IPAddresses = info.getHostAddresses();
	        				break;
	        			}
	        		}
	        		
	        		jmdns.close();
	            }
	        }
	    }		
		
		if (IPAddresses == null || IPAddresses.length < 1) {
			throw new UnknownHostException("Impossible to find " + bonjourName + " over the network");
		}
		
		return IPAddresses[0];
	}
}
